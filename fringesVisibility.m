% Code to calculate fringes visibility

lambda = 1550*10^3; % [pm]
c = 3*10^5; % [pm/fs]
sigma = 90; % [fs]
t0 = 0; % [fs]

Resolution = logspace(0, 10, 2*1000); %[pm]
% Resolution = [-fliplr(Resolution)];
Time = logspace(-1, 7, 2*500); %[fs]
NTime = -fliplr(Time);

[R, T] = meshgrid( Resolution, Time );
[~,NT] = meshgrid( Resolution, NTime );

W =  ( ( lambda^2 ./ ( R * c ) ) .*...
    ( ( lambda^2 ./ ( R * c ) ) + 2 .* ( T - t0 ) ) ) ./ ( 2 * sigma^2 );

NW = ( ( lambda^2 ./ ( R * c ) ) .*...
    ( ( lambda^2 ./ ( R * c ) ) + 2 .* ( NT - t0 ) ) ) ./ ( 2 * sigma^2 );

Intensity = sech( W );
NIntensity = sech( NW );

figure('Position',[100, 100, 800, 600])
[ha, pos] = tight_subplot(2,1,[0.03 0.01],0.1,0.1);
axes(ha(1));
h = pcolor(Resolution, Time, Intensity);
h.EdgeColor = 'none';
set(gca,'YDir','normal')
set(gca,'xscale','log')
set(gca,'yscale','log')
set(gca, 'XAxisLocation', 'top')
% title('Widoczność oscylacji','FontSize',21)
axes(ha(2));
g = pcolor(Resolution, NTime, NIntensity);
g.EdgeColor = 'none';
set(gca,'YDir','normal')
set(gca,'xscale','log')
set(gca,'yscale','log')
xlabel('Rozdzielczość [pm]','FontSize',18)
height=pos{1}(2)+pos{1}(4)-pos{2}(2);
h3=axes('position',[pos{2}(1)-0.01 pos{2}(2) pos{2}(3) height],'visible','off');
h_label=ylabel('\Delta t [fs]','visible','on','FontSize',18);