% distances = containers.Map('KeyType','int32','ValueType','int32');

estNumOfBins = 20;
photonsToCheck = 5000;

Counts = zeros(1,estNumOfBins+1);

directory = '/home/misimik/Dokumenty/Magisterka/Magisterka/Photon/Spektrometr/measurements';
cd(directory)

period = 12500; % [ps]
binWidth = 10; % [ps]
bins = uint32(period/binWidth);
windowWidth = 300;
syncDivider = 1;
% Calib shift comes from CalibrationCable - it is number of bins the
% photons are shifted
% -------------------------------------------
% For 10km about 3937 bins
% For 2km + 15m about 811 bins
% For 15m about 2 bins
% For 2km + 4F +15m about 812 bin
% For 10km +15m in 1st channel and 2km in 2nd channel about 3127 bins
% -------------------------------------------
calibShift = 0;

% Open and read the file
[filename, pathname]=uigetfile('*.tttr3', 'T-Mode data:');
fid=fopen([pathname filename]);
A = uint64(fread(fid,'uint32'));
% Create masks to read certain bit positions of the records in file
ChannelMask = uint64(bin2dec('0 111111 000 000 000 000 000 0000 000 000'));
SSTimeMask = uint64(bin2dec('0 000 000 111 111 111 111 111 0000 000 000'));
SyncCountMask = uint64(bin2dec('0 000 000 000 000 000 000 000 1111 111 111'));
FlagMask = uint64(bin2dec('1 000 000 000 000 000 000 000 0000 000 000'));
%  The mask selects the bits, and the shift ensures that no following zeros
%  are present
Channel = bitshift( bitand( A, ChannelMask ), -25 );
SSTime = bitshift( bitand( A, SSTimeMask ), -10 );
SyncCount = bitshift( bitand( A, SyncCountMask ), 0 );
Flag = bitshift( bitand( A, FlagMask ), -31 );
clearvars A
if numel(unique(Channel)) >= 3
    % Create Channel selectors
    C1 = ( Channel == 0 );
    C2 = ( Channel == 1 );
    S = ( Channel == 63 );
    % Using SyncCount overflow Flag we introduce infinite sync counter - the
    % flag works as a divider clock
    ExtendedCount = syncDivider *...
        ( uint64(cumsum(Flag.*SyncCount)) * 1024 + uint64( SyncCount.*mod(Flag+1,2) ) ) + uint64( floor( SSTime ./ period ) )  ;
    clearvars Flag
    % Shifting channels according to calibration
    ExtendedCount(C2) = ExtendedCount(C2)+calibShift;
    [ExtendedCount,ind] = sort(ExtendedCount);
    C1 = C1(ind);
    C2 = C2(ind);
    S = S(ind);
    SSTime = SSTime(ind);
    % We wrap Start-Stop times to period of single sync
    SSTime = mod(SSTime, period);
    % Throw out the Sync events - they are not needed anymore and they could
    % interfere
    ExtendedCount = ExtendedCount( ~S );
    SSTime = SSTime( ~S );
    C1 = C1( ~S );
    C2 = C2( ~S );
end

for i = 1:photonsToCheck
    
    if C2(i)
       
        difference = 0;
        j = i;
        
        while j<photonsToCheck
           
            j = j+1;
            
            if C1(j)
                
                %fprintf(1,'In loop\n')
               
                difference = abs( ExtendedCount(j) - ExtendedCount(i) );
                
                %fprintf(1,'%d\t%d\t%d\n',i,j,difference)
                
                if difference < estNumOfBins
                   Counts(difference+1) = Counts(difference+1) + 1;
                else
                    break
                end
                
            end
            
        end
        
    end
    
end

figure
plot(0:estNumOfBins,Counts,'.')
[m,i] = max(Counts)