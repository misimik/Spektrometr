%% Initialize
close all;
%clear all;
directory = '/home/misimik/Dokumenty/Magisterka/Magisterka/Photon/Spektrometr/';
cd(directory);

%% Import data OSA

[Wavelength1,Power1] = importOSA_period(strcat(directory,'measurements/OSA_PM_interference_maximum.csv'));
% [Wavelength2,Power2] = importOSA_period(strcat(directory,'measurements/OSA_4f_NO_SLIT.csv'));

PowerN1 = Power1./max(Power1);
% PowerN2 = Power2./max(Power2);

% PowerN1 = Power2./Power1*100/4.53*0.24;

clearvars wavelengthOUT wavelengthIN OUT IN

%% Fitting Gaussian
options = fitoptions('gauss1','Lower',[0 1552 0],'Upper',[Inf 1556 Inf]);
[fitobject, gof, output] = fit(Wavelength1, PowerN1, 'gauss1',options);

%% Plot Transmission

figure('Position',[100, 100, 1200, 750])
hold on;
plot(Wavelength1, PowerN1, 'b-');
plot(Wavelength1, fitobject(Wavelength1), 'k+');
% plot(Wavelength2, PowerN2, 'r-');
set(gca,'fontsize',18);
ax = gca;
ax.XLim = [1552 1557]
title('Granica rozdzielczości linii 4f','fontsize',24);
ylabel('Transmisja [j.u.]','fontsize',22);
xlabel('Długość fali [nm]','fontsize',22);
legend({'Transmisja najwęższej szczeliny', 'Dopasowana krzywa'},'FontSize',18);
% str = sprintf('f(x) =  %g *\nexp(-((x-%g)/%g)^2)\n+ %g * \nexp(-((x-%g)/%g)^2) ',...
%     fitobject.a1,fitobject.b1,fitobject.c1,fitobject.a2,fitobject.b2,fitobject.c2);
str = sprintf('f(x) =  %g *\nexp(-((x-%g)/%g)^2)\sinn',...
    fitobject.a1,fitobject.b1,fitobject.c1);
annotation('textbox',[0.56 0.5 0.3 0.3],'String',str,'FitBoxToText','on','FontSize',18);

%%
Power = Power1./Power2;
Power = Power(4001:42000);
FT = fftshift(fft(Power./max(Power)));
N = numel(Power);
%Wavelength = (SSTimeHist1Save - SSTimeHist1Save(i))/-4945+2*779;
Wavelength = Wavelength1(4001:42000);
FT = abs( FT(end/2+1:end) + FT(end/2:-1:1) );
FT = FT/N ;
dlambda = Wavelength(2)-Wavelength(1);
f_s = 1/dlambda;
df = f_s/N;
f = (0:df:f_s/2-df);
plot(f,FT);
