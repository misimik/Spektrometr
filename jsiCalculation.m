% Author: Michal Mikolajczyk
% This code may not be distributed or modified without author's permission.
% 24.08.2017

close all
clear all

directory = '/home/misimik/Dokumenty/Magisterka/Magisterka/Photon/Spektrometr/';
cd(directory)

period = 12500; % [ps]
binWidth = 10; % [ps]
bins = uint32(period/binWidth);
windowWidth = 3000;
syncDivider = 1;
% Calib shift comes from CalibrationCable - it is number of bins the
% photons are shifted
% -------------------------------------------
% For 10km about 3936 bins
% For 2km + 15m about 811 bins
% For 15m about 2 bins
% For 2km + 4F +15m about 812 bin
% For 10km +15m in 1st channel and 2km in 2nd channel about 3127 bins
% -------------------------------------------
calibShift = 13;

% Open and read the file
[filename, pathname]=uigetfile('*.tttr3', 'T-Mode data:');
fid=fopen([pathname filename]);
A = uint64(fread(fid,'uint32'));
% Create masks to read certain bit positions of the records in file
ChannelMask = uint64(bin2dec('0 111111 000 000 000 000 000 0000 000 000'));
SSTimeMask = uint64(bin2dec('0 000 000 111 111 111 111 111 0000 000 000'));
SyncCountMask = uint64(bin2dec('0 000 000 000 000 000 000 000 1111 111 111'));
FlagMask = uint64(bin2dec('1 000 000 000 000 000 000 000 0000 000 000'));
%  The mask selects the bits, and the shift ensures that no following zeros
%  are present
Channel = bitshift( bitand( A, ChannelMask ), -25 );
SSTime = bitshift( bitand( A, SSTimeMask ), -10 );
SyncCount = bitshift( bitand( A, SyncCountMask ), 0 );
Flag = bitshift( bitand( A, FlagMask ), -31 );
clearvars A ChannelMask FlagMask SSTimeMask SyncCountMask
if numel(unique(Channel)) >= 3
    % Create Channel selectors
    C1 = ( Channel == 0 );
    C2 = ( Channel == 1 );
    S = ( Channel == 63 );
    clearvars Channel
    % Using SyncCount overflow Flag we introduce infinite sync counter - the
    % flag works as a divider clock
    ExtendedCount = syncDivider *...
        ( cumsum(Flag.*SyncCount) * 1024 + SyncCount ) ;%+ floor( SSTime ./ period );
    clearvars Flag
    % Shifting channels according to calibration
    ExtendedCount(C2) = ExtendedCount(C2)+calibShift;
    [ExtendedCount,ind] = sort(ExtendedCount);
    C1 = C1(ind);
    C2 = C2(ind);
    S = S(ind);
    SSTime = SSTime(ind);
    % We wrap Start-Stop times to period of single sync
    % SSTime = mod(SSTime, period);
    % Throw out the Sync events - they are not needed anymore and they could
    % interfere
    ExtendedCount = ExtendedCount( ~S );
    SSTime = SSTime( ~S );
    C1 = C1( ~S );
    C2 = C2( ~S );
    
    % Locate channel maxima
    C1Hist = histcounts( double( SSTime( C1 ) ), bins );
    C2Hist = histcounts( double( SSTime( C2 ) ), bins );
    [ ~ , idx1 ] = findpeaks(C1Hist,'MinPeakProminence', max(C1Hist)/1.5 );
    peakTime1 = ( idx1 - 0.5 ) * binWidth;
    [ ~ , idx2 ] = findpeaks(C2Hist,'MinPeakProminence', max(C2Hist)/1.5 );
    peakTime2 = ( idx2 - 0.5 ) * binWidth;

    % Coincidence happens when both channels click in the same sync period
    Coincidence = [ ( ExtendedCount(1:end-1) == ExtendedCount(2:end) ); 0 ];
    % We mark both of events that took part in the coincidence
    Coincidence = or(Coincidence(1:end), [0;Coincidence(1:end-1)] );

    % Create Channel specific Coincidence selectors
    CC1 = and( C1, Coincidence );
    CC2 =  and( C2, Coincidence );

    % Look for double clicks on the same detector (because dead time of the
    % detectors is smaller than the laser repetition rate
    DoubleC1 = and( CC1, [ 0; CC1(1:end-1) ] );
    DoubleC2 = and( CC2, [ 0; CC2(1:end-1) ] );
    DoubleC1 = or(DoubleC1, circshift( DoubleC1, -1 ) );
    DoubleC2 = or(DoubleC2, circshift( DoubleC2, -1 ) );
    % Remove the single detector double clicks
    CC1 = xor( CC1, DoubleC1 );
    CC2 = xor( CC2, DoubleC2 );

    figure('Position',[100, 100, 800, 600])
    histogram( double(SSTime(C1)), bins );
    hold on
    histogram( double(SSTime(C2)), bins );
    hold off
    title('Photon Count')
    xlabel('Time of Arrival [ps]')
    ylabel('Counts')

    figure('Position',[100, 100, 800, 600])
    histogram( double( SSTime( CC1 ) ), bins );
    hold on
    histogram( double( SSTime( CC2 ) ), bins );
    hold off
    title('Coincidences Count')
    xlabel('Time of Arrival [ps]')
    ylabel('Counts')
    
    SSTime = double(SSTime);
        
    JSI = reshape(SSTime(logical(CC1+CC2)),[2,sum(CC1)]);
    if calibShift > 0
        JSI = flipud(JSI);
    end  
    [H2,XEdges,YEdges] = myHist( JSI(1,:), JSI(2,:), 10, 10 );
    XEdges = (XEdges - peakTime1)/171.9+1558;
    YEdges = (YEdges - peakTime2)/164+1558;
    figure('Position',[100, 100, 800, 600])
    imagesc('XData',XEdges(2:end)-0.01,'YData',YEdges(2:end)-0.05,'CData',H2);
    set(gca,'YDir','normal');
    set(gca,'fontsize',14)
    ax = gca;
    ax.XLim = [1545 1570]
    ax.YLim = [1545 1570]
    ax.XTick = 1545:5:1570;
    ax.YTick = 1545:5:1570;
    title('Joint Spectral Intensity - wide nm pump')
    ylabel('Spectrum of channel two (2km) [nm]')
    xlabel('Spectrum of channel one (10km) [nm]')
    
    
else
    fprintf(2,'\nChanell error - not all channels are present\n');
end
