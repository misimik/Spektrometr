%close all
clear all

directory = '/home/misimik/Dokumenty/Magisterka/Magisterka/Photon/Spektrometr/measurements';
cd(directory)

period = 12500; % [ps]
binWidth = 10; % [ps]
bins = uint64(period/binWidth);
windowWidth = 300;
syncDivider = 1;
% Calib shift comes from CalibrationCable - it is number of bins the
% photons are shifted
% -------------------------------------------
% For 10km about 3937 bins
% For 2km + 15m about 811 bins
% For 15m about 2 bins
% For 2km + 4F +15m about 812 bin
% For 10km +15m in 1st channel and 2km in 2nd channel about 3127 bins
% -------------------------------------------
calibShift = 14;

% Open and read the file
[filename, pathname]=uigetfile('*.tttr3', 'T-Mode data:');
fid=fopen([pathname filename]);
A = uint64(fread(fid,'uint32'));
% Create masks to read certain bit positions of the records in file
ChannelMask = uint64(bin2dec('0 111111 000 000 000 000 000 0000 000 000'));
SSTimeMask = uint64(bin2dec('0 000 000 111 111 111 111 111 0000 000 000'));
SyncCountMask = uint64(bin2dec('0 000 000 000 000 000 000 000 1111 111 111'));
FlagMask = uint64(bin2dec('1 000 000 000 000 000 000 000 0000 000 000'));
%  The mask selects the bits, and the shift ensures that no following zeros
%  are present
Channel = bitshift( bitand( A, ChannelMask ), -25 );
SSTime = bitshift( bitand( A, SSTimeMask ), -10 );
SyncCount = bitshift( bitand( A, SyncCountMask ), 0 );
Flag = bitshift( bitand( A, FlagMask ), -31 );
if numel(unique(Channel)) >= 3
    % Create Channel selectors
    C1 = ( Channel == 0 );
    C2 = ( Channel == 1 );
    S = ( Channel == 63 );
    % Using SyncCount overflow Flag we introduce infinite sync counter - the
    % flag works as a divider clock
    ExtendedCount = syncDivider *...
        ( uint64(cumsum(Flag.*SyncCount)) * 1024 + uint64( SyncCount.*mod(Flag+1,2) ) ) + uint64( floor( SSTime ./ period ) )  ;
    % Shifting channels according to calibration
    ExtendedCount(C2) = ExtendedCount(C2)+calibShift;
    [ExtendedCount,ind] = sort(ExtendedCount);
    C1 = C1(ind);
    C2 = C2(ind);
    S = S(ind);
    Flag = Flag(ind);
    SSTime = SSTime(ind);
    % We wrap Start-Stop times to period of single sync
    SSTime = mod(SSTime, period);
    % Throw out the Sync events - they are not needed anymore and they could
    % interfere
    ExtendedCount = ExtendedCount( ~S );
    SSTime = SSTime( ~S );
    C1 = C1( ~S );
    C2 = C2( ~S );
    % Locate channel maxima
    C1Hist = histcounts( double( SSTime( C1 ) ), bins );
    C2Hist = histcounts( double( SSTime( C2 ) ), bins );
    [ ~ , idx1 ] = findpeaks(C1Hist,'MinPeakProminence', max(C1Hist)/1.5 );
    peakTime1 = ( idx1 - 0.5 ) * binWidth;
    [ ~ , idx2 ] = findpeaks(C2Hist,'MinPeakProminence', max(C2Hist)/1.5 );
    peakTime2 = ( idx2 - 0.5 ) * binWidth;
    % Create in-peak coincidences selector
    InTimeClick1 = ( ( SSTime < ( peakTime1 + windowWidth/2 ) ) & ( SSTime > ( peakTime1 -windowWidth/2 ) ) );
    InTimeClick2 = ( ( SSTime < ( peakTime2 + windowWidth/2 ) ) & ( SSTime > ( peakTime2 -windowWidth/2 ) ) );
    C1inT = and( C1, InTimeClick1 );
    C2inT = and( C2, InTimeClick2 );
    C12inT = or( C1inT, C2inT );
    C1inT = C1inT( C12inT );
    C2inT = C2inT( C12inT );
    % Throwout events that coincide with dark counts
    ExtendedCountInT = ExtendedCount( C12inT );
    SSTimeInT = SSTime( C12inT );

    % Coincidence happens when both channels click in the same sync period
    % Double12 = [or( and( C1inT(1:end-1), C2inT(2:end) ), and( C1inT(2:end), C2inT(1:end-1) ) ); 0];
    Coincidence = [ ( ExtendedCountInT(1:end-1) == ExtendedCountInT(2:end) ); 0 ];% .* Double12;
    % We mark both of events that took part in the coincidence
    Coincidence = or(Coincidence(1:end), [0;Coincidence(1:end-1)] );

    % Create Channel specific Coincidence selectors
    CC1inT = and( C1inT, Coincidence );
    CC2inT = and( C2inT, Coincidence );

    % Look for double clicks on the same detector (because dead time of the
    % detectors is smaller than the laser repetition rate
    DoubleC1 = and( CC1inT, [ 0; CC1inT(1:end-1) ] );
    DoubleC2 = and( CC2inT, [ 0; CC2inT(1:end-1) ] );
    DoubleC1 = or(DoubleC1, circshift( DoubleC1, -1 ) );
    DoubleC2 = or(DoubleC2, circshift( DoubleC2, -1 ) );
    % Remove the single detector double clicks
    CC1inT = xor( CC1inT, DoubleC1 );
    CC2inT = xor( CC2inT, DoubleC2 );

    figure('Position',[100, 100, 800, 600])
    histogram( double(SSTime(C1)), bins );
    hold on
    histogram( double(SSTime(C2)), bins );
    hold off
    title('Photon Count')
    xlabel('Time of Arrival [ps]')
    ylabel('Counts')

    figure('Position',[100, 100, 800, 600])
    histogram( double( SSTimeInT( CC1inT ) ), bins );
    hold on
    histogram( double( SSTimeInT( CC2inT ) ), bins );
    hold off
    title('Coincidences Count')
    xlabel('Time of Arrival [ps]')
    ylabel('Counts')

    g2 = ExtendedCount(end)*uint64(sum(CC2inT)*1000)/(uint64(sum(C1inT))*uint64(sum(C2inT)))
else
    fprintf(2,'\nChanell error - not all channels are present\n');
end
