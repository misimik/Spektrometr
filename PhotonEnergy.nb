(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      7389,        198]
NotebookOptionsPosition[      6788,        172]
NotebookOutlinePosition[      7117,        187]
CellTagsIndexPosition[      7074,        184]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
   "Number", " ", "of", " ", "photons", " ", "in", " ", "a", " ", "beam"}], 
   " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"n", " ", "=", " ", "100000"}], ";"}], "  ", 
   RowBox[{"(*", " ", 
    RowBox[{"Number", " ", "of", " ", "photons", " ", "per", " ", "second"}], 
    " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Pb", " ", "=", " ", 
     RowBox[{"7.4", "*", 
      RowBox[{"10", "^", 
       RowBox[{"-", "3"}]}]}]}], ";", " ", 
    RowBox[{"(*", " ", 
     RowBox[{"Beam", " ", "power", " ", "in", " ", "Watts"}], " ", "*)"}], 
    "\[IndentingNewLine]", 
    RowBox[{"\[HBar]", " ", "=", " ", 
     RowBox[{"1.0545718", "*", 
      RowBox[{"10", "^", 
       RowBox[{"-", "34"}]}]}]}], ";", " ", 
    RowBox[{"(*", " ", 
     RowBox[{"J", "*", "s"}], " ", "*)"}], "\[IndentingNewLine]", 
    RowBox[{"\[Lambda]", " ", "=", " ", 
     RowBox[{"1550", "*", 
      RowBox[{"10", "^", 
       RowBox[{"-", "9"}]}]}]}], ";", " ", 
    RowBox[{"(*", " ", "m", " ", "*)"}], "\[IndentingNewLine]", 
    RowBox[{"c", " ", "=", " ", 
     RowBox[{"3", "*", 
      RowBox[{"10", "^", "8"}]}]}], ";", 
    RowBox[{"(*", " ", 
     RowBox[{"m", "/", "s"}], " ", "*)"}], "\[IndentingNewLine]", 
    RowBox[{"\[Omega]", " ", "=", " ", 
     RowBox[{"2", "*", "Pi", "*", 
      RowBox[{"c", "/", "\[Lambda]"}]}]}], ";", " ", 
    RowBox[{"(*", " ", 
     RowBox[{"Rad", "/", "s"}], " ", "*)"}], "\[IndentingNewLine]", 
    RowBox[{"Eph", " ", "=", " ", 
     RowBox[{"\[HBar]", "*", "\[Omega]"}]}], " ", 
    RowBox[{"(*", " ", "J", " ", "*)"}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"StringForm", "[", 
    RowBox[{"\"\<Energy of a single photon is: ``J\>\"", ",", 
     RowBox[{"ScientificForm", "[", 
      RowBox[{"Eph", ",", "5"}], "]"}]}], "]"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"P", " ", "=", " ", 
     RowBox[{"n", "*", "Eph"}]}], " ", 
    RowBox[{"(*", " ", "W", " ", "*)"}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"StringForm", "[", 
    RowBox[{
    "\"\<Beam power of `` photons per second is ``W\>\"", ",", "n", ",", 
     RowBox[{"ScientificForm", "[", 
      RowBox[{"P", ",", "5"}], "]"}]}], "]"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"OD", " ", "=", " ", 
     RowBox[{"-", 
      RowBox[{"Log10", "[", 
       RowBox[{"P", "/", "Pb"}], "]"}]}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"StringForm", "[", 
    RowBox[{
    "\"\<Filter Optical Density should be ``, which is equall to ``dB\>\"", 
     ",", 
     RowBox[{"NumberForm", "[", 
      RowBox[{"OD", ",", "3"}], "]"}], ",", 
     RowBox[{"NumberForm", "[", 
      RowBox[{
       RowBox[{"10", "*", "OD"}], ",", "4"}], "]"}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.737082392532969*^9, 3.737082426422255*^9}, {
  3.737082496099296*^9, 3.737082521165436*^9}, {3.7370826174165382`*^9, 
  3.737082880689406*^9}, {3.7370829440829487`*^9, 3.7370830472705603`*^9}, {
  3.737083100900416*^9, 3.7370831782159557`*^9}, {3.737083347877357*^9, 
  3.7370834898514233`*^9}, {3.73708365847269*^9, 3.737083674885789*^9}, {
  3.737085908337945*^9, 3.737086151269416*^9}, {3.7371881133806257`*^9, 
  3.737188115349332*^9}}],

Cell[BoxData[
 InterpretationBox["\<\"Energy of a single photon is: \\!\\(\\*RowBox[{\\\"\\\
\\\\\"1.2825\\\\\\\"\\\", \\\"\[Times]\\\", SuperscriptBox[\\\"10\\\", \\\"\\\
\\\\\"-19\\\\\\\"\\\"]}]\\)J\"\>",
  StringForm["Energy of a single photon is: ``J", 
   ScientificForm[1.2824651688630831`*^-19, 5]],
  Editable->False]], "Output",
 CellChangeTimes->{
  3.737083136346324*^9, 3.737083170671218*^9, {3.737083480039843*^9, 
   3.737083490882465*^9}, {3.737083665266452*^9, 3.737083675679101*^9}, {
   3.737086106851708*^9, 3.737086151887521*^9}, 3.7371881197818623`*^9}],

Cell[BoxData[
 InterpretationBox["\<\"Beam power of \\!\\(\\*RowBox[{\\\"100000\\\"}]\\) \
photons per second is \\!\\(\\*RowBox[{\\\"\\\\\\\"1.2825\\\\\\\"\\\", \\\"\
\[Times]\\\", SuperscriptBox[\\\"10\\\", \\\"\\\\\\\"-14\\\\\\\"\\\"]}]\\)W\"\
\>",
  StringForm["Beam power of `` photons per second is ``W", 100000, 
   ScientificForm[1.282465168863083*^-14, 5]],
  Editable->False]], "Output",
 CellChangeTimes->{
  3.737083136346324*^9, 3.737083170671218*^9, {3.737083480039843*^9, 
   3.737083490882465*^9}, {3.737083665266452*^9, 3.737083675679101*^9}, {
   3.737086106851708*^9, 3.737086151887521*^9}, 3.737188119799863*^9}],

Cell[BoxData[
 InterpretationBox["\<\"Filter Optical Density should be \
\\!\\(\\*RowBox[{\\\"\\\\\\\"11.8\\\\\\\"\\\"}]\\), which is equall to \\!\\(\
\\*RowBox[{\\\"\\\\\\\"117.6\\\\\\\"\\\"}]\\)dB\"\>",
  StringForm["Filter Optical Density should be ``, which is equall to ``dB", 
   NumberForm[11.761186141016113`, 3], 
   NumberForm[117.61186141016113`, 4]],
  Editable->False]], "Output",
 CellChangeTimes->{
  3.737083136346324*^9, 3.737083170671218*^9, {3.737083480039843*^9, 
   3.737083490882465*^9}, {3.737083665266452*^9, 3.737083675679101*^9}, {
   3.737086106851708*^9, 3.737086151887521*^9}, 3.73718811980521*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"2.14", "+", "2.13", "+", "2.15", "+", "2.17", "+", 
  "2.14"}]], "Input",
 CellChangeTimes->{{3.737188136796536*^9, 3.737188146609013*^9}, {
  3.737190474541223*^9, 3.737190477556787*^9}}],

Cell[BoxData["10.73`"], "Output",
 CellChangeTimes->{3.7371881475263863`*^9, 3.7371904781487226`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"(", 
  RowBox[{
   RowBox[{"Pb", "/", "Eph"}], "*", 
   RowBox[{"10", "^", 
    RowBox[{"-", "10.73"}]}]}], ")"}]], "Input",
 CellChangeTimes->{{3.737188166133048*^9, 3.73718816730369*^9}, {
  3.7371882054414682`*^9, 3.73718826572755*^9}, {3.7371904805325727`*^9, 
  3.737190483709949*^9}, {3.7371913987036448`*^9, 3.7371914143242693`*^9}, {
  3.737202069024086*^9, 3.737202150973938*^9}, {3.737202751196509*^9, 
  3.7372027520957212`*^9}}],

Cell[BoxData["1.074449828802821`*^6"], "Output",
 CellChangeTimes->{{3.7371882509109907`*^9, 3.7371882667805767`*^9}, 
   3.7371904841694813`*^9, {3.737191400871406*^9, 3.73719141495883*^9}, {
   3.737202072886031*^9, 3.7372021513845263`*^9}, 3.737202753257327*^9}]
}, Open  ]]
},
WindowSize->{808, 911},
WindowMargins->{{371, 101}, {-289, 72}},
FrontEndVersion->"11.0 for Linux x86 (64-bit) (September 21, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 3227, 77, 332, "Input"],
Cell[3810, 101, 574, 10, 67, "Output"],
Cell[4387, 113, 632, 11, 34, "Output"],
Cell[5022, 126, 628, 11, 34, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5687, 142, 212, 4, 32, "Input"],
Cell[5902, 148, 101, 1, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6040, 154, 464, 10, 32, "Input"],
Cell[6507, 166, 265, 3, 34, "Output"]
}, Open  ]]
}
]
*)

