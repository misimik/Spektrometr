classdef experimentSetup
    %experimentSetup This class contains information about conditions in
    %which the experiment was held extracted from PTU file header.
    
    properties
        numberOfInputChannels;
        timeResolution; %   ps
        syncRate;       %   Hz
        inputRate;      %   Hz
        numberOfRecords;
        duration;       %   ms
    end
    
    methods 
        function obj = experimentSetup(val)
            if nargin > 0
                obj.timeResolution = val.timeResolution;
            end
        end
    end
    
end

