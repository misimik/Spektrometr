function dataStatus = dataStatus( fid )
%dataStatus - This functions if the data is useful.
% This is done by checking if the data contains measurements from at least
% two channels.

A = uint64(fread(fid,10^7,'uint32'));
% Create masks to read certain bit positions of the records in file
ChannelMask = uint64(bin2dec('0 111111 000 000 000 000 000 0000 000 000'));
%  The mask selects the bits, and the shift ensures that no following zeros
%  are present
Channel = bitshift( bitand( A, ChannelMask ), -25 );

dataStatus = true;

if numel(unique(Channel)) < 3
   dataStatus = false;
   error('Error.\nNotEnough working detector channels.','');
end
clearvars A ChannelMask
end

