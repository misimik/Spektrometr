%% Initialize
directory = 'D:\MichalM\Spektrometr\CleanCode';
%directory = '/home/misimik/Dokumenty/Magisterka/Magisterka/Photon/Spektrometr/CleanCode';
cd(directory)
resolution = 20;    %[ps]
period = 12500; % [ps]
% Calib shift comes from CalibrationCable - it is number of bins the
% photons are shifted
% -------------------------------------------
% For 10km about 3937 bins
% For 2km + 15m about 812 bins
% For 15m about 2 bins
% For 2km + 4F +15m about 812 bin
% For 10km +15m in 1st channel and 2km in 2nd channel about 3127 bins
% -------------------------------------------
%% Open and read the file
[filename, pathname]=uigetfile('*.ptu', 'T-Mode data:');
fid=fopen([pathname filename]);
frewind(fid);
experimentSetup = readHeader(experimentSetup,fid);

bins = uint32(period/experimentSetup.timeResolution);
Edges = [(0-eps:resolution.*experimentSetup.timeResolution:period-eps) period-eps]';
fclose(fid);
%% Calibration
fid=fopen([pathname filename]);
estNumOfBins = 5000;
photonsToCheck = 50000;
Counts = zeros(1,estNumOfBins+1);
[ C1,C2,ExtendedCount,SSTime ] = getData(fid, period, 0, experimentSetup.timeResolution);
for i = 1:photonsToCheck
    
    if C2(i)       
        difference = 0;
        j = i;
        
        while j<photonsToCheck           
            j = j+1;
            
            if C1(j)                               
                difference = abs( ExtendedCount(j) - ExtendedCount(i) );
                               
                if difference < estNumOfBins
                   Counts(difference+1) = Counts(difference+1) + 1;
                else
                    break
                end 
            end 
        end
    end
end

[max1, ind1] = max(Counts);
Counts(ind1) = 0;
[max2, ind2] = max(Counts);
Counts(ind1) = max1;
    
    if and(max1 < 4*max2, abs(ind1 - ind2) == 1 )
       fprintf(2, 'Pik wychodzi poza krawędź binu\n');
    end

calibShift = ind1-1;

clearvars i j photonsToCheck C1 C2 SSTime estNumOfBins ExtendedCount difference %ind1 ind2 ind max1 max2
fclose(fid);
%% Extract pairs

C1SpectSave = [];
C2SpectSave = [];
SSTimeHist1Save = [];
SSTimeHist2Save = [];
JSI = [];

C1SpectAliasing = zeros(1,313);

for i = 1:16
    
    C1long = logical([0]);
    C2long = logical([0]);
    CC1long = logical([0]);
    SSTimelong = double([0]);
    ExtendedCountlong = uint64([0]);
    
    fid = fopen([pathname filename(1:end-7) sprintf('%03.0f',i-1) '.ptu']);
    
    frewind(fid);
    skipHeader(fid);
    dataStatus(fid);
    frewind(fid);
    skipHeader(fid);
    
%     When the extraction started:
%     fprintf(1,strcat('The extraction started at:\t',datestr(datetime('now')),'\n'));
    
     while ~feof(fid)
%     for i = 1:10
%         Make a marking to show progress
%         fprintf(1,'* ');
%         Read data from file:
        [ C1,C2,ExtendedCount,SSTime ] = getData(fid, period, calibShift, experimentSetup.timeResolution);
        [CC1,CC2,ExtendedCount,SSTime] = getCoincidences(C1,C2,ExtendedCount,SSTime);    
                     
        CC1long = [CC1long; CC1];
          
        clearvars CC1 CC2 SSTime C1 C2        
     end
     
     L = length(CC1long);
     fprintf(1,'%i\n',L)

    fclose(fid);
end

%% Spectral polarization tomography

Ro = zeros(4,4,length(Edges)-1,length(Edges)-1);
Norm = zeros(length(Edges)-1,length(Edges)-1);
Vsave = zeros(1,length(Edges)-1,length(Edges)-1);

for j=1:16
    
    fid = fopen([pathname filename(1:end-7) sprintf('%03.0f',j) '.ptu']);

    C1SpectSave = [];
    C2SpectSave = [];
    SSTimeHist1Save = [];
    SSTimeHist2Save = [];
    JSI = [];

    for calibShift = calibShift-0:1:calibShift+0

        C1long = logical([0]);
        C2long = logical([0]);
        CC1long = logical([0]);
        CC2long = logical([0]);
        SSTimelong = double([0]);
        ExtendedCountlong = uint64([0]);

        frewind(fid);

        skipHeader(fid);
        dataStatus(fid);
        frewind(fid);
        skipHeader(fid);

    %     When the extraction started:
        fprintf(1,strcat('The extraction started at:\t',datestr(datetime('now')),'\n'));

         while ~feof(fid)
    %     for i = 1:10
    %         Make a marking to show progress
            fprintf(1,'* ');
    %         Read data from file:
            [ C1,C2,ExtendedCount,SSTime ] = getData(fid, period, calibShift, experimentSetup.timeResolution);
            [CC1,CC2,ExtendedCount,SSTime] = getCoincidences(C1,C2,ExtendedCount,SSTime);    

            CC1long = [CC1long; CC1];
            CC2long = [CC2long; CC2];
            SSTimelong = [SSTimelong; SSTime];
            % ExtendedCount is calculated within one while loop and needs
            % correction.
            ExtendedCountlong = [ ExtendedCountlong; ( ExtendedCount + ExtendedCountlong( end ) ) ];

            clearvars CC1 CC2 SSTime C1 C2        
         end

        %     T = table(ExtendedCountlong,SSTimelong,CC1long,CC2long);
    %     writetable( T, strcat( pathname, filename( 1 : end - 4 ), '_coincidences.csv' ),...
    %         'Delimiter' , ',' , 'QuoteStrings' , true)
    %     clearvars T
    %     
    %     fprintf('\n');
    %     
    %     [C1Spect, SSTimeHist1] = histcounts( double( SSTimelong (CC1long) ), Edges);
    % %     clearvars CC1long
    %     [C2Spect, SSTimeHist2] = histcounts( double( SSTimelong (CC2long) ), Edges);
    % %     clearvars CC2long
    % 
    %     Time = (Edges + [ Edges(2:end); 0 ])./2;
    %     Time = Time(1:end-1);
    %     Time = Time;
    %     C1Spect = C1Spect';
    %     C2Spect = C2Spect';
    %     
    %     T = table(Time,C1Spect,C2Spect);
    %     writetable( T, strcat( pathname, filename( 1 : end - 4 ), '_histogram.csv' ),...
    %         'Delimiter' , ',' , 'QuoteStrings' , true)
    %     clearvars T

        JSI = [JSI, [SSTimelong(CC1long),SSTimelong(CC2long)]'];
        
        h = histogram2(JSI(1,:),JSI(2,:),Edges,Edges,'DisplayStyle','tile');
        V = h.Values;
        Vsave(j,:,:) = V;
        

    end
    
    Tmp = kron(M(j),V);
    Ro = Ro + reshape(Tmp,size(Ro));    
    if j<4
       Norm = Norm+V; 
    end    
    fclose(fid);    
    clear JSI Tmp V h
end

for l1=1:4
    for l2=1:4
        for k1=1:length(Edges)-1
            for k2=1:length(Edges)-1
                if Norm(k1,k2)~=0
                    Ro(l1,l2,k1,k2) = Ro(l1,l2,k1,k2)./Norm(k1,k2);
                end                
            end
        end
    end
end

%% Saving Data
 
h5create( [filename(1:end-4) '_stateMatrix.h5'], '/Matrix/real', size(Ro), 'Datatype', 'double', 'Deflate', 9, 'ChunkSize', [4 4 16 16] );
h5write( [filename(1:end-4) '_stateMatrix.h5'], '/Matrix/real', real(Ro));
h5create( [filename(1:end-4) '_stateMatrix.h5'], '/Matrix/imag', size(Ro), 'Datatype', 'double', 'Deflate', 9, 'ChunkSize', [4 4 16 16] );
h5write( [filename(1:end-4) '_stateMatrix.h5'], '/Matrix/imag', imag(Ro));
h5create( [filename(1:end-4) '_stateMatrix.h5'], '/Edges', size(Edges), 'Datatype', 'double');
h5write( [filename(1:end-4) '_stateMatrix.h5'], '/Edges', Edges);

%%
for j=1:16
   j1 = floor((j-0.1)/4)+1;
   j2 = mod(j-1,4)+1;
   A = squeeze( real(Ro( j1, j2, :, : ) ) );
   maximum = max( max( A ) )
   [x,y] = find( A == maximum )
   
end

