function normalized = nonZeroNormalize(toNormalize,Normalizing)

    normalized = (toNormalize./Normalizing);
    normalized( isnan( normalized ) ) = 0;
    normalized( isinf( normalized ) ) = 0;

end

