function [ Histogram, XEdges, YEdges ] = myHist( XData, YData, xBinWidth, yBinWidth )

% Author: Michal Mikolajczyk
% This code may not be distributed or modified without author's permission.
% 24.08.2017
    
%     xMin = min( XData );
%     xMax = xMax( XData );
%     yMin = min( YData );
%     yMax = xMax( YData );

    if yBinWidth < xBinWidth
       
        tmp = yBinWidth;
        yBinWidth = xBinWidth;
        xBinWidth = tmp;
        
        tmp = YData;
        YData = XData;
        XData = tmp;
        
        
    end
    
    XEdges = ( min( XData ) - xBinWidth/2 ):xBinWidth:( max( XData ) + xBinWidth/2 );
    YEdges = ( min( YData ) - yBinWidth/2 ):yBinWidth:( max( YData ) + yBinWidth/2 );
    
    Histogram = zeros( numel( XEdges )-1, numel( YEdges )-1 );
    
    for ye = 1:numel( YEdges )-1
        
        Histogram( : , ye ) = histcounts(  XData( YData >= YEdges( ye ) & ...
            YData < YEdges( ye + 1 ) ), XEdges );
        
    end
        
    if yBinWidth < xBinWidth
       
        Histogram = Histogram';
        tmp = YEdges;
        YEdges = XEdges;
        XEdges = YEdges;
        
    end 
    
    Histogram = Histogram';
end