function [ Click1,Click2,ExtendedCount,SSTime ] = getData( fid, period, calibShift, timeResolution )
%getData Reads binary files and returns data for further processing

A = uint64(fread(fid,10^7,'uint32'));
% Create masks to read certain bit positions of the records in file
ChannelMask = uint64(bin2dec('0 111111 000 000 000 000 000 0000 000 000'));
SSTimeMask = uint64(bin2dec('0 000 000 111 111 111 111 111 0000 000 000'));
SyncCountMask = uint64(bin2dec('0 000 000 000 000 000 000 000 1111 111 111'));
FlagMask = uint64(bin2dec('1 000 000 000 000 000 000 000 0000 000 000'));
%  The mask selects the bits, and the shift ensures that no following zeros
%  are present
Channel = bitshift( bitand( A, ChannelMask ), -25 );
SSTime = bitshift( bitand( A, SSTimeMask ), -10 ).*uint64(timeResolution);
SyncCount = bitshift( bitand( A, SyncCountMask ), 0 );
Flag = bitshift( bitand( A, FlagMask ), -31 );

% Create Channel selectors
Click1 = ( Channel == 0 );
Click2 = ( Channel == 1 );
S = ( Channel == 63 ); % Synchronization channel
% Using SyncCount overflow Flag we introduce infinite sync counter - the
% flag works as a divider clock
ExtendedCount = ( uint64(cumsum(Flag.*SyncCount)) *...
    1024 + uint64( SyncCount.*mod(Flag+1,2) ) ) + uint64( floor( SSTime ./ period ) );
% We wrap Start-Stop times to period of single sync
SSTime = mod(SSTime, period);
% Shifting channels according to calibration
ExtendedCount(Click2) = ExtendedCount(Click2)+calibShift;
[ExtendedCount,ind] = sort(ExtendedCount);
Click1 = Click1(ind);
Click2 = Click2(ind);
SSTime = SSTime(ind);
S = S(ind);
% Throw out the Sync events - they are not needed anymore and they could
% interfere
ExtendedCount = ExtendedCount( ~S );
SSTime = SSTime( ~S );
Click1 = Click1( ~S );
Click2 = Click2( ~S );
end

