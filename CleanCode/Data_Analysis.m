measurementPeriod = 0.004; %(nm)
%directory = '~/Dokumenty/Doktorat/Photon/Spectral_interference/';
directory = 'D:\MichalM\Spektrometr\CleanCode';
cd(directory)

[filename, pathname]=uigetfile('*.csv', 'T-Mode data:');
[Transmission_wavelengths, Transmission_intensity] = readOSA([pathname 'Transmission_' filename]);
[Wavelengths, Intensity] = readOSA([pathname filename]);

NormIntensity = nonZeroNormalize( Intensity, interp1( Transmission_wavelengths,...
    Transmission_intensity, Wavelengths ) );

IntensityFT = fftshift( fft( NormIntensity ) );
freqNM = ( -( length( IntensityFT ) /2 ):1:( ( length( IntensityFT ) -1 )/2 ) )./( Wavelengths(end)-Wavelengths(1));
% figure;
plot( 1./freqNM, ( abs( IntensityFT ) ), 'DisplayName', filename(26:end-4) );