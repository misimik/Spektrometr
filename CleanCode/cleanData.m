function [C1,C2,ExtendedCount,SSTime] = cleanData(Channel,Flag,SSTime,SyncCount,period)
%cleanData Extracts data for processing

% Create Channel selectors
    C1 = ( Channel == 0 );
    C2 = ( Channel == 1 );
    S = ( Channel == 63 ); % Synchronization channel
    % Using SyncCount overflow Flag we introduce infinite sync counter - the
    % flag works as a divider clock
    ExtendedCount = ( uint64(cumsum(Flag.*SyncCount)) *...
        1024 + uint64( SyncCount.*mod(Flag+1,2) ) ) + uint64( floor( SSTime ./ period ) );
    [ExtendedCount,ind] = sort(ExtendedCount);
    C1 = C1(ind);
    C2 = C2(ind);
    S = S(ind);
    SSTime = SSTime(ind);
    % We wrap Start-Stop times to period of single sync
    SSTime = mod(SSTime, period);
    % Throw out the Sync events - they are not needed anymore and they could
    % interfere
    ExtendedCount = ExtendedCount( ~S );
    SSTime = SSTime( ~S );
    C1 = C1( ~S );
    C2 = C2( ~S );
end

