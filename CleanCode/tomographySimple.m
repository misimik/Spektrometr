[filename, pathname]=uigetfile('*.csv', 'Tomography 16 measurements data:');

delimiter = ',';
startRow = 2;
formatSpec = '%f%f%[^\n\r]';

fileID = fopen([pathname filename],'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
fclose(fileID);
NumberOfCoincidences = dataArray{:, 2};
clearvars filename delimiter startRow formatSpec fileID dataArray ans;

Ro = zeros(4,4);
norm = sum(NumberOfCoincidences(1:4));

for i = 1:16
   Ro = Ro + M(i)*NumberOfCoincidences(i)  ;
end
Ro = Ro./norm;

%% Plot
figure('Position',[100, 100, 1050, 850]);
b = bar3(real(Ro));
colorbar;
for k = 1:length(b)
    zdata = b(k).ZData;
    b(k).CData = zdata;
    b(k).FaceColor = 'interp';
end
yticklabels({'VV','VH','HV','HH'});
xticklabels({'VV','VH','HV','HH'});
title('Density matrix - real');

figure('Position',[100, 100, 1050, 850]);
d = bar3(imag(Ro));
colorbar;
for k = 1:length(d)
    zdata = d(k).ZData;
    d(k).CData = zdata;
    d(k).FaceColor = 'interp';
end
yticklabels({'VV','VH','HV','HH'});
xticklabels({'VV','VH','HV','HH'});
title('Density matrix - imaginary');