%% Initialize
%directory = 'D:\MichalM\Spektrometr\CleanCode';
directory = '/home/misimik/Dokumenty/Doktorat/Photon/Spektrometr/CleanCode';
cd(directory)

period = 12500; % [ps]
% Calib shift comes from CalibrationCable - it is number of bins the
% photons are shifted
% -------------------------------------------
% For 10km about 3937 bins
% For 2km + 15m about 812 bins
% For 15m about 2 bins
% For 2km + 4F +15m about 812 bin
% For 10km +15m in 1st channel and 2km in 2nd channel about 3127 bins
% -------------------------------------------
%% Open and read the file
[filename, pathname]=uigetfile('*.ptu', 'T-Mode data:');
fid=fopen([pathname filename]);
frewind(fid);
experimentSetup();
experimentSetup = readHeader(experimentSetup,fid);

bins = uint32(period/experimentSetup.timeResolution);
Edges = [(0-eps:experimentSetup.timeResolution:period-eps) period-eps]';
%% Calibration

estNumOfBins = 5000;
photonsToCheck = 50000;
Counts = zeros(1,estNumOfBins+1);
[ C2,C1,ExtendedCount,SSTime ] = getData(fid, period, 0, experimentSetup.timeResolution);
for i = 1:photonsToCheck
    
    if C2(i)       
        difference = 0;
        j = i;
        
        while j<photonsToCheck           
            j = j+1;
            
            if C1(j)                               
                difference = abs( ExtendedCount(j) - ExtendedCount(i) );
                               
                if difference < estNumOfBins
                   Counts(difference+1) = Counts(difference+1) + 1;
                else
                    break
                end 
            end 
        end
    end
end

[max1, ind1] = max(Counts);
Counts(ind1) = 0;
[max2, ind2] = max(Counts);
Counts(ind1) = max1;
    
    if and(max1 < 4*max2, abs(ind1 - ind2) == 1 )
       fprintf(2, 'Pik wychodzi poza krawędź binu\n');
    end

calibShift = ind1-1;
[ ~, calibShifts ] = findpeaks(Counts,'MinPeakProminence',mean(Counts)*1.5);

clearvars i j photonsToCheck C1 C2 SSTime estNumOfBins ExtendedCount difference %ind1 ind2 ind max1 max2
%% Extract pairs

C1SpectSave = [];
C2SpectSave = [];
SSTimeHist1Save = [];
SSTimeHist2Save = [];
JSI = [];

C1SpectAliasing = zeros(1,313);
C1MultiplePeaks = zeros(1,numel(calibShiftShifts));
C2MultiplePeaks = zeros(1,numel(calibShiftShifts));

%for calibShift = calibShift-0:1:calibShift+0
for i = 1:numel(calibShifts)
    
    calibShift = calibShifts(i)
    
    C1long = logical([0]);
    C2long = logical([0]);
    CC1long = logical([0]);
    CC2long = logical([0]);
    SSTimelong = double([0]);
    ExtendedCountlong = uint64([0]);
    
    frewind(fid);
    skipHeader(fid);
    dataStatus(fid);
    frewind(fid);
    skipHeader(fid);
    
%     When the extraction started:
    fprintf(1,strcat('The extraction started at:\t',datestr(datetime('now')),'\n'));
    
     while ~feof(fid)
%     for i = 1:10
%         Make a marking to show progress
        fprintf(1,'* ');
%         Read data from file:
        [ C2,C1,ExtendedCount,SSTime ] = getData(fid, period, calibShift, experimentSetup.timeResolution);
        [CC1,CC2,ExtendedCount,SSTime] = getCoincidences(C1,C2,ExtendedCount,SSTime);    
                     
        CC1long = [CC1long; CC1];
        CC2long = [CC2long; CC2];
        SSTimelong = [SSTimelong; SSTime];
        % ExtendedCount is calculated within one while loop and needs
        % correction.
        ExtendedCountlong = [ ExtendedCountlong; ( ExtendedCount + ExtendedCountlong( end ) ) ];
          
        clearvars CC1 CC2 SSTime C1 C2        
     end
     
     C1MultiplePeaks(i) = histcounts( double( SSTimelong( CC1long ) ), bins );
     C2MultiplePeaks(i) = histcounts( double( SSTimelong( CC2long ) ), bins );

end



%% Rework on subset of coincidences

% [filename, pathname]=uigetfile('*.csv', 'T-Mode data:');
% [ExtendedCountlong,SSTimelong,CC1long,CC2long] = readCoincidences(strcat(pathname,filename));

timeOfMeasurement = 600; % [minutes]
peakParameters = zeros(timeOfMeasurement,4);

for i = 1:timeOfMeasurement
    fprintf(1,'* ');
    endExtendedCount = i*60*80*10^6;
    startExtendedCount = (i-1)*60*80*10^6;
    Select = (ExtendedCountlong < endExtendedCount) .* (ExtendedCountlong >= startExtendedCount);
    CC1long_tmp = CC1long.*(Select);
    [C1Spect_tmp, SSTimeHist1] = histcounts( double( SSTimelong( logical(CC1long_tmp)) ), Edges);
%     [~,ind] = max(C1SpectC_tmp);
%     options = fitoptions('gauss1','Lower',[0 5000 0],'Upper',[10^5 7000 Inf]);
%     [fitobjects, gof, output] = fit(SSTimeHist1, C1Spect_tmp','gauss1',options);
%     peakPosition(i,:) = [ i; fitobject.b1 ];
    tmp = ezfit(SSTimeHist1(1:end-1), C1Spect_tmp,'gauss');
    peakParameters(i,:) = [ i; tmp.m(1); tmp.m(2); tmp.m(3) ];
end



%% 

 % Locate channel maxima
    [C1Hist, B1] = histcounts( double( SSTimelong( CC1long ) ), bins );
    clearvars C1long
    [C2Hist, B2] = histcounts( double( SSTimelong( CC2long ) ), bins );
    clearvars C2long
    [ ~ , idx1 ] = findpeaks(C1Hist,'MinPeakProminence', max(C1Hist)/3 );
    peakTime1 = ( idx1 - 0.5 ) * binWidth;
    [ ~ , idx2 ] = findpeaks(C2Hist,'MinPeakProminence', max(C2Hist)/3 );
    peakTime2 = ( idx2 - 0.5 ) * binWidth;
        
    JSI = reshape(SSTimelong(logical(CC1long+CC2long)),[2,sum(CC1long)]);
    if calibShift > 0
        JSI = flipud(JSI);
    end
    
    [C1Spect, SSTimeHist1] = histcounts( double( SSTimelong (CC1long) ), bins);
    clearvars CC1long
    [C2Spect, SSTimeHist2] = histcounts( double( SSTimelong (CC2long) ), bins);
    clearvars CC2long
    
    
 %% Rysowanie
 
%     figure('Position',[100, 100, 800, 600])
%     histogram( double(SSTimelong(C1long)), bins );
%     hold on
%     histogram( double(SSTimelong(C2long)), bins );
%     hold off
%     title('Photon Count')
%     xlabel('Time of Arrival [ps]')
%     ylabel('Counts')
% 
%     figure('Position',[100, 100, 800, 600])
%     histogram( double( SSTimelong( CC1long ) ), bins );
%     hold on
%     histogram( double( SSTimelong( CC2long ) ), bins );
%     hold off
%     title('Coincidences Count')
%     xlabel('Time of Arrival [ps]')
%     ylabel('Counts')
    
    [H2,XEdges,YEdges] = myHist( JSI(1,:), JSI(2,:), binWidth, binWidth );
    XEdges = double(XEdges); YEdges = double(YEdges);
    [~, Xidx] = max(max(H2));
    [~, Yidx] = max(H2(:,Xidx));
    XEdges = (XEdges - XEdges(Xidx))/164+1554.6;
    YEdges = (YEdges - YEdges(Yidx))/164+1554.6;

%     XEdges = (XEdges - bbb)/167+1558.4;
%     YEdges = (YEdges - ccc)/167+1558.4;

    figure('Position',[100, 100, 1050, 850])
    imagesc('XData',XEdges(2:end)-0.01,'YData',YEdges(2:end)-0.05,'CData',H2);
    c = colorbar;
    set(gca,'YDir','normal');
    set(gca,'fontsize',18)
    ax = gca;
    ax.XLim = [1540 1575.1]
    ax.YLim = [1540 1575.1]
    ax.XTick = 1540:5:1575.1;
    ax.YTick = 1540:5:1575.1;
    title('�?ączne widmo - \Delta \lambda_{pompa} = 0.36nm, 9,7km','fontsize',24)
    ylabel('Widmo kanału 1 [nm]','fontsize',22)
    xlabel('Widmo kanału 2 [nm]','fontsize',22)
    c.Label.String = 'Liczba zliczeń';
    c.Label.FontSize = 22;

%% Fitting Gaussian
figure('Position',[100, 100, 1200, 750])
hold on
options778 = fitoptions('gauss3','Lower',[0 -4e4 0 0 -2e4 0 0 0e4 0], 'Upper',[Inf -2e4 Inf Inf 0e4 Inf Inf 2e4 Inf]);
[fitobject, gof, output] = fit(SSTimeHist2Save', C2SpectSave', 'gauss3',options778);
plot(SSTimeHist1Save,C1SpectSave,'b.');
% plot(SSTimeHist1Save7785,C1SpectSave7785,'r.');
plot(SSTimeHist1Save,fitobject(SSTimeHist1Save),'r-');
set(gca,'fontsize',18);
str = sprintf('f(x) =  %g *\nexp(-((x-%g)/%g)^2)\n+ %g * \nexp(-((x-%g)/%g)^2) + %g * \nexp(-((x-%g)/%g)^2) ',...
    fitobject.a1,fitobject.b1,fitobject.c1,fitobject.a2,fitobject.b2,fitobject.c2,...
    fitobject.a3,fitobject.b3,fitobject.c3);
annotation('textbox',[0.4 0.6 0.3 0.3],'String',str,'FitBoxToText','on','FontSize',18);

%% Helper

Wavelength779 = (SSTimeHist1Save778-SSTimeHist1Save778(1330))/-4945+2*778.46;
Wavelength778 = (SSTimeHist1Save779-SSTimeHist1Save779(1439)+period)/-4945+2*779.56;

% options = fitoptions('gauss3','Lower',[0 769 0 0 772 0 0 777 0], 'Upper',[Inf 772 Inf Inf 777 Inf Inf 780  Inf]);
% [fitobject, gof, output] = fit(Wavelength27785', C1SpectSave7785', 'gauss3',options);
figure('Position',[100, 100, 1200, 750])
hold on;
plot(Wavelength778,C1SpectSave778,'b.');
plot(Wavelength779,C1SpectSave779,'r.');
% plot(Wavelength17785,fitobject(Wavelength17785),'k-');
hold off;
set(gca,'fontsize',18);
% str = sprintf('f(x) =  %g *\nexp(-((x-%g)/%g)^2)\n+ %g * \nexp(-((x-%g)/%g)^2) + %g * \nexp(-((x-%g)/%g)^2) ',...
%     fitobject.a1,fitobject.b1,fitobject.c1,fitobject.a2,fitobject.b2,fitobject.c2,...
%     fitobject.a3,fitobject.b3,fitobject.c3);
% annotation('textbox',[0.4 0.6 0.3 0.3],'String',str,'FitBoxToText','on','FontSize',18);
title('Spectral Shift of Generated Photons - CFBG','fontsize',24);
ylabel('Number of Counts','fontsize',22);
xlabel('Wavelength [nm]','fontsize',22);
legend({'Pump @ 779.55nm', 'Pump @ 778.46nm'},'FontSize',18);
%% Helper Spect
[~,i] = max(C1SpectSave);

Wavelength = (SSTimeHist1Save - SSTimeHist1Save(i))/-4945+2*779;
figure('Position',[100, 100, 1200, 750]);
plot(Wavelength, C1SpectSave);
set(gca,'fontsize',18);
ax = gca;
ax.XLim = [1557.5 1559.5];
title('Spectral interference','fontsize',24);
ylabel('Counts','fontsize',22);
xlabel('Wavelength [nm]','fontsize',22);
% legend({'Pump @ 779.55nm', 'Pump @ 778.46nm'},'FontSize',18);

%% FFT

FT = fftshift(fft(C1SpectSave./max(C1SpectSave)));
N = numel(C1SpectSave);
Wavelength = (SSTimeHist1Save - SSTimeHist1Save(i))/-4945+2*779;
FT = abs( FT(end/2+1:end) + FT(end/2:-1:1) );
FT = FT/N ;
dlambda = double(20/4945);
f_s = 1/dlambda;
df = f_s/N;
f = (0:df:f_s/2-df);
plot(f,FT);


%%
%% Fitting Gaussian
figure('Position',[100, 100, 1200, 750])
hold on

XData = SSTimeHist2Save;
YData = C2SpectSave;

[~,i] = max(YData);

options778 = fitoptions('gauss1','Lower',[0 -1e4 0 0 0e4 0 0 2e4 0], 'Upper',[Inf 0e4 Inf Inf 2e4 Inf Inf 4e4 Inf]);
[fitobject, gof, output] = fit(XData', YData', 'gauss1');
plot(XData,YData,'b.');
% plot(SSTimeHist1Save7785,C1SpectSave7785,'r.');
plot(XData,fitobject(XData),'r-');
set(gca,'fontsize',18);
ax = gca;
ax.XLim = [-100 100];
title('Rozkład czasowy impulsu niepoddanego dyspersji','fontsize',24);
xlabel('Czas [ps]','fontsize',22);
ylabel('Liczba zliczeń','fontsize',22);
str = sprintf('f(x) =  %g *\nexp(-((x-%g)/%g)^2)\n',...
    fitobject.a1,fitobject.b1,fitobject.c1);
annotation('textbox',[0.58 0.5 0.3 0.3],'String',str,'FitBoxToText','on','FontSize',18);


%% Spectrum straightening

directory2 = '/home/misimik/Dokumenty/Magisterka/Magisterka/Photon/Spektrometr/';
% cd(directory2);

[Wavelength,Power] = importOSA_period(strcat(directory2,'measurements/Fiber Transmission/OSA_9700m.csv'));
[WavelengthBackground,PowerBackground] = importOSA_period(strcat(directory2,'measurements/Fiber Transmission/OSA_0m.csv'));

if sum(Wavelength == WavelengthBackground) == numel(Wavelength)
    PowerNormalized = Power ./ PowerBackground;
    plot(Wavelength, PowerNormalized)
else fprintf(2, 'Data from files is not compatible\n')
end

%% Two moved peaks

b778 = 2*778.46;
b779 = 2*779.56;
D = -4954;

options778 = fitoptions('gauss1','Lower',[0 1.3e5 0 ], 'Upper',[Inf 1.5e5 Inf]);
[fitobject778, gof778, output778] = fit(SSTimeHist1Save778', C1SpectSave778', 'gauss1',options778);
options779 = fitoptions('gauss1','Lower',[0 1.3e5 0 ], 'Upper',[Inf 1.5e5 Inf]);
[fitobject779, gof779, output779] = fit(SSTimeHist1Save779', C1SpectSave779', 'gauss1',options779);

Wavelength778 = (SSTimeHist1Save778-fitobject778.b1)/D+b778;
Wavelength779 = (SSTimeHist1Save779-fitobject779.b1)/D+b779;

c778 = abs(fitobject778.c1/D);
c779 = abs(fitobject779.c1/D);

gauss = @( x, x0, sigma ) exp( -( (x-x0)/sigma).^2 );

f = figure('Position',[100, 100, 1200, 750])
hold on;
plot(Wavelength778,C1SpectSave778,'b.');
plot(Wavelength778,fitobject778.a1.*gauss(Wavelength778,b778,c778),'b--','LineWidth',2);
plot(Wavelength779,C1SpectSave779,'r.');
plot(Wavelength779,fitobject779.a1.*gauss(Wavelength779,b779,c779),'r--','LineWidth',2);
hold off;
set(gca,'fontsize',18);
xlim([1551 1565])
ax = gca;
line([b778 b778],[0 ax.YLim(2)],'LineStyle','--');
line([b779 b779],[0 ax.YLim(2)],'LineStyle','--');

% str = sprintf('f(x) =  %g *\nexp(-((x-%g)/%g)^2)\n+ %g * \nexp(-((x-%g)/%g)^2) + %g * \nexp(-((x-%g)/%g)^2) ',...
%     fitobject.a1,fitobject.b1,fitobject.c1,fitobject.a2,fitobject.b2,fitobject.c2,...
%     fitobject.a3,fitobject.b3,fitobject.c3);
% annotation('textbox',[0.4 0.6 0.3 0.3],'String',str,'FitBoxToText','on','FontSize',18);

str1 = sprintf('f(x) =  %g * \nexp(-((x-%g)/%g)^2) ',fitobject778.a1,b778,c778);
str2 = sprintf('f(x) =  %g * \nexp(-((x-%g)/%g)^2) ',fitobject779.a1,b779,c779);

title('Przesunięcie widma','fontsize',24);
ylabel('Liczba zliczeń','fontsize',22);
xlabel('Długość fali [nm]','fontsize',22);
legend({'Pompa @ 778.46nm', str1,'Pompa @ 779.56nm',str2},'FontSize',14,'Location','northwest');

%% Anti-diagonal sumation

% a - a square matrix
lena = size(a,1);
AntidiagonalCrossection = zeros(2*lena,1);
for i=1:lena
    for j=1:i
        AntidiagonalCrossection(i)=AntidiagonalCrossection(i)+a(j,lena-i+j);
        AntidiagonalCrossection(2*lena+1-i)=AntidiagonalCrossection(2*lena+1-i)+a(lena-i+j,j);
    end
end

%% Diagonal sumation

% a - a square matrix
lena = size(a,1);
DiagonalCrossection = zeros(2*lena,1);
for i=1:lena
    for j=1:i
        DiagonalCrossection(i)=DiagonalCrossection(i)+a(j,i+1-j);
        DiagonalCrossection(2*lena+1-i)=DiagonalCrossection(2*lena+1-i)+a(lena-i+j,lena+1-j);
    end
end