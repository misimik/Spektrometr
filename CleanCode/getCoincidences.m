function [Coincidence1,Coincidence2,ExtendedCount,SSTime] = getCoincidences(Click1,Click2,ExtendedCount,SSTime)
%getCoincidences returns coincidences from raw clicks data

% First eliminate doublet clicks on the same channel in the same time bin
    ClickTime1 = ExtendedCount(Click1);
    ClickTime2 = ExtendedCount(Click2);
    DoubleClick1 = [ ( ClickTime1(1:end-1) == ClickTime1(2:end) ); 0 ];
    DoubleClick2 = [ ( ClickTime2(1:end-1) == ClickTime2(2:end) ); 0 ];
% We mark both of events that took part in the faulty coincidence, and take
% the good ones.
    DoubleClick1 = ~or( DoubleClick1(1:end), [0; DoubleClick1( 1:end-1 )]);
    DoubleClick2 = ~or( DoubleClick2(1:end), [0; DoubleClick2( 1:end-1 )]);
% We select only proper events
    Click1(Click1) = DoubleClick1;
    Click2(Click2) = DoubleClick2;
% We purify the extended count
    SELECT = or( Click1, Click2 );
    ExtendedCount = ExtendedCount( SELECT );
    SSTime = SSTime( SELECT );
    Click1 = Click1( SELECT );
    Click2 = Click2( SELECT );
% Coincidence happens when both channels click in the same sync period
    Coincidence = [ ( ExtendedCount(1:end-1) == ExtendedCount(2:end) ); 0 ];
% We mark both of events that took part in the coincidence
    Coincidence = or(Coincidence(1:end), [0;Coincidence(1:end-1)] );
% Create Channel specific Coincidence selectors
    Coincidence1 = and( Click1, Coincidence );
    Coincidence2 =  and( Click2, Coincidence );
% Reduce data to necessary miminum
    SELECT = or(Coincidence1,Coincidence2);
    Coincidence1=Coincidence1(SELECT);
    Coincidence2=Coincidence2(SELECT);
    SSTime = SSTime(SELECT);
    ExtendedCount = ExtendedCount(SELECT);
    if sum(Coincidence1)~=sum(Coincidence2)
        error('Error.\nUnbalanced clicks 2.','');
    end
end

